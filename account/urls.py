from django.urls import path, include
from .views import index, django_signup, django_login
from django.conf.urls import url

app_name = 'account'

urlpatterns = [
    path('', index, name='index'),
    path('signup/', django_signup, name='signup'),
    path('login/', django_login, name='login'),
]