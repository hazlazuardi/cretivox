from django.shortcuts import redirect, render, reverse
from django.contrib.auth import authenticate, login
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .forms import UserForm, UserCreateForm
from django.contrib import messages
from django.shortcuts import redirect, render, reverse
from django.http import HttpResponse

# Create your views here.


def index(request):
    if request.method == "GET":
        return render(request, "djaccindex.html")


# Similar to Story 10

# function 1: Sign up

def django_signup(request):
    f = UserCreateForm(request.POST)
    context = {
        'signupform': f
    }
    if request.method == "GET":
        return render(request, "djaccsignup.html", context)
    if request.method == 'POST':
        if f.is_valid():
            f.save()
            username = request.POST['username']
            password1 = request.POST['password1']
            context = {
                'username': username,
                'password': password1
            }
            return render(request, "djacclogin.html", context) 
    else:
        f = UserCreateForm()
 
    return render(request, 'djaccsignup.html', context)

# Function 2: Login
    # Initialize a user's Cart
    # Redirect to landing

def django_login(request):            
    context = {
        'loginform': UserForm()
    }
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if(user is not None):
            request.session['username'] = username
            login(request, user)
            request.method = "GET"
            return redirect(reverse('landing:landing'))
        else:
            return render(request, "djacclogin.html", context)
    return render(request, "djacclogin.html", context)