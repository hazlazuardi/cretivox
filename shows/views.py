from django.http import request
from django.shortcuts import render

# Create your views here.

def shows(request):
    context = {
        'titleCardSmall' : 'Latest Episode',
        'titleCardVideo' : 'All Episodes'
    }
    return render(request, 'showParent.html', context)

def showsDetail(request):
    return render(request, 'showDetail.html')