from django.contrib import admin
from django.urls import path

from .views import shows, showsDetail

app_name = 'shows'

urlpatterns = [
    path('', shows, name='shows'),
    path('show-detail/', showsDetail)
]
