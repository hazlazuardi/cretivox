from django.contrib import admin
from django.urls import path

from .views import *

app_name = 'articles'

urlpatterns = [
    path('', articleParent, name='articlesParent'),
    path('sports/', sportsParent, name='sportsParent'),
    path('tech/', techParent, name='techParent'),
    path('lifestyle/', lifestyleParent, name='lifestyleParent'),
    path('food/', foodParent, name='foodParent'),
    path('celeb/', celebParent, name='celebParent'),
    # path('sports/', sportsParent),

]
