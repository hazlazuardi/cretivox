from django.http import request
from django.shortcuts import render

# Create your views here.
def articleParent(request):
    return render(request, 'articleParent.html')

def sportsParent(request):
    context = {
        'forYouTitle' : 'Popular in Sports',
        'topNewsTitle' : 'Latest in Sports'
    }
    return render(request, 'categoryParent.html', context)

def techParent(request):
    context = {
        'forYouTitle' : 'Popular in Tech',
        'topNewsTitle' : 'Latest in Tech'
    }
    return render(request, 'categoryParent.html', context)

def lifestyleParent(request):
    context = {
        'forYouTitle' : 'Popular in Lifestyle',
        'topNewsTitle' : 'Latest in Lifestyle'
    }
    return render(request, 'categoryParent.html', context)

def celebParent(request):
    context = {
        'forYouTitle' : 'Popular in Celeb',
        'topNewsTitle' : 'Latest in Celeb'
    }
    return render(request, 'categoryParent.html', context)

def foodParent(request):
    context = {
        'forYouTitle' : 'Popular in Food',
        'topNewsTitle' : 'Latest in Food'
    }
    return render(request, 'categoryParent.html', context)
