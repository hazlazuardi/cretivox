from django.shortcuts import render

# Create your views here.
def landing(request):
    context = {
        'titleCardSmall' : 'Top News',
        'titleCardVideo' : 'Shows'
    }
    return render(request, 'landingParent.html', context)

def articleParent(request):
    return render(request, 'articleParent.html')