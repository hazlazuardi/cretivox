from django.contrib import admin
from django.urls import path

from .views import articleParent, landing

app_name = 'landing'

urlpatterns = [
    path('', landing, name='landing'),
    path('articles/', articleParent)
]
